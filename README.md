#EVALUATION MMI2 : mise en application jQuery
> **Avant de commencer :**
> - Vous disposez d'une heure et demie pour effectuer les consignes de l'évaluation.
> - Une fois terminé, appelez votre enseignant pour la correction de votre travail.
> - Vous trouverez sous ces consignes des méthodes jQuery qui pourront vous aider.
> - Aidez-vous de la [documentation jQuery](http://api.jquery.com/){:target="_blank"} !
> - Usez et abusez des console.log() / alert() pour vous aider.
> - Bon courage !

----------

### **Consignes de l'évaluation** :

- Remplacez l'appel de **jQuery via CDN** par un jQuery **local** et placez cet appel de manière optimale dans votre page.
- Effectuez l'ensemble des consignes suivantes dans un fichier **app.js**. Celui-ci doit exécuter du code javascript uniquement lorsque le DOM est prêt.
- Au click sur le logo, charger le contenu de la page **/page/contenu.html** de manière asynchrone dans **#pages**. Si vous n'y arrivez pas, copier-coller le contenu de **/page/contenu.html** dans **#pages** pour les consignes suivantes. 
- Afficher après .logo le nombre de liens présent dans le contenu. 
- Enveloppez tous les liens avec la balise ```<strong>```.
- Lors d'un click sur l'un des liens present dans le contenu, lui ajouter la classe **'active'**. Seuls les liens cliqués doivent avoir la classe **'active'**.
- Lors d'un click sur l'un des liens present dans le contenu, afficher en alert() l'url associée avec le texte suivant : ```Le lien est le suivant : ```, sans charger celui-ci.
- Récupérer le texte en gras present dans le contenu chargé précédemment, et les afficher sous forme de liste, **après** le paragraphe dans **#liste**.
- A l'aide du plugins **[mark.js](https://markjs.io/)**, mettre en avant le terme 'plop' si celui-ci est présent dans la définition.
- BONUS : Si un paragraphe contient un 'plop', afficher le paragraphe en rouge.

### **Méthodes jQuery** :

**toLowerCase()** // **contain()** // **next()** // **is()** // **wrapInner()** // **wrap()** // **each()** // **append()** // **prepend()** // **html()** // **text()** // **attr()** // **remove()** // **hide()** // **show()** // **$(this)** // **$.get('url',function(){ ... })** // **click(function(){ ... })** // **if(...){...}**

