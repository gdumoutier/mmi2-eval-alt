<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>MMI2 EVALUATION</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Mono" rel="stylesheet">
</head>
<body>
<div class="layout remodal-bg">
	<header role="banner">
		<div class="wrapper" data-relative-input="true" id="scene">
			<p class="logo" data-depth="0.6">
				Évaluation
			</p>
		</div>
	</header>
	<main>
		<div class="wrapper">
			<div id="page">
				<article>
					<div>
					
					</div>
					</article>
			</div>
			<hr>
			<ul id="liste"></ul>
			<hr>
			<p>
				<strong>Expliquez en quelques mots la methode jQuery <i>end()</i></strong> :
				<!-- Votre réponse ici -->
			</p>
		</div>
	</main>
	<footer>
		<div class="wrapper">
			<p>Pied de page</p>
		</div>
	</footer>
</div>

<div class="remodal" data-remodal-id="contact">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Contact</h1>
	<p class="letter-section">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ipsam itaque minima, nesciunt numquam, odio officia quia quo quod recusandae tempore totam voluptatem, voluptatum. Minima molestias nemo nihil perferendis ullam.</p>
	<br>
	<form action="">
		<div class="form-group"><label for="name">Nom</label><input id="name" name="name" type="text" class="form-control"></div>
		<div class="form-group"><label for="mail">Email</label><input id="mail" name="mail" type="text" class="form-control"></div>
		<div class="form-group"><label for="message">Votre message</label><textarea name="message" id="message" cols="20" rows="5" class="form-control"></textarea></div>
	</form>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Envoyer</button>
</div>

<script src="https://use.fontawesome.com/601a2e39a6.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</body>
</html>